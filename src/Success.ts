import Try, { Options, Func } from "./Try";
import Failure from "./Failure";
import __ from '../index';

export default class Success<T> extends Try<T> {
    private _value: T;
    private _opts: Options;

    public constructor(value: T, opts?: Options) {
        super();
        this._value = value;
        this._opts = opts || {};
    }

    public get isSuccess(): boolean {
        return true;
    }

    public get isFailure(): boolean {
        return false;
    }

    public get value(): T {
        return this._value;
    }

    public get(): T {
        return this.value;
    }

    public getOrDefault(): T {
        return this.value;
    }

    public getOrElse(other: T): T {
        return this.value;
    }

    public orElse(other: Func<T>): Try<T> {
        return this;
    }

    public map<U>(mapper: Func<T, U>): Try<U> {
        return __<U>(() => mapper(this.value), this._opts);
    }

    public flatMap<U>(mapper: Func<T, Try<U>>): Try<U> {
        return __<U>(() => mapper(this.value), this._opts);
    }

    public recover(recover: Func<Error, T>): Try<T> {
        return this;
    }

    public filter(filter: Func<T>): Try<T> {
        try {
            let result = filter(this.value);
            if(result) {
                return this;
            }

            return new Failure<T>("There are no results from applying the filter");
        } catch(e) {
            return new Failure<T>("There are no results from applying the filter");
        }
    }

    public set(setter: Func<T>): Try<T> {
        try {
            setter(this.value);
            return this;
        } catch(e) {
            return new Failure<T>(e);
        }
    }
}