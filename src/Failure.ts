import Try, { Options, Func } from "./Try";
import Success from "./Success";
import __ from '../index';

export default class Failure<T> extends Try<T> {
    private _value: Error;
    private _opts: Options;

    public constructor(value: Error | string, opts?: Options) {
        super(); 
        if(typeof(value) === "string") {
            value = new Error(value);
        }
        this._value = value;
        this._opts = opts || {};
    }

    public get isSuccess(): boolean {
        return false;
    }

    public get isFailure(): boolean {
        return true;
    }

    public get value(): any {
        return this._value;
    }

    public get(): T {
        throw this.value;
    }

    public getOrDefault(): T {
        if(this._opts.class && this._opts.class.default && typeof this._opts.class.default === 'function') {
            return this._opts.class.default();
        }
        return this.value;
    }

    public getOrElse(other: T): T {
        return other;
    }

    public orElse(other: Func<T>): Try<T> {
        return __<T>(other, this._opts);
    }

    public map<U>(mapper: Func<T, U>): Try<U> {
        return new Failure<U>(this.value, this._opts);
    }

    public flatMap<U>(mapper: Func<T, Try<U>>): Try<U> {
        return new Failure<U>(this.value, this._opts);
    }

    public recover(recover: Func<Error, T>): Try<T> {
        try {
            return new Success<T>(recover(this.value), this._opts);
        } catch(e) {
            return new Failure<T>(this.value, this._opts);
        }
    }

    public filter(filter: Func<T>): Try<T> {
        return this;
    }

    public set(setter: Func<T>): Try<T> {
        return this;
    }
}