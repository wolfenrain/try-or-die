export interface Func<T> extends Function {}
export interface Func<T,U=void> extends Function {}
export type Options = { isPromise?: boolean, class?: any }

export default abstract class Try<T> {
    /**
     * Returns whether a Try is a Success (true) or a Failure (false).
     */
    public abstract get isSuccess(): boolean;
    
    /**
     * Returns whether a Try is a Failure (true) or a Success (false).
     */
    public abstract get isFailure(): boolean;
    
    /**
     * Returns the value of the Try if it is a Success, and throws an exception if it is a Failure.
     */
    public abstract get value(): T|any;

    /**
     * Gets the value of the Try if it is a Success, and throws an exception if it is a Failure.
     */
    public abstract get(): T;

    /**
     * Returns the value of the Try if it is a Success, and the default value of T if it is a Failure.
     */
    public abstract getOrDefault(): T|any;
    
    /**
     * Returns the value of the Try if it is a Success, or the other parameter if it is a Failure.
     * 
     * @param other
     */
    public abstract getOrElse(other: T): T|any;

    /**
     * Returns the value of the Try if it is a Success, or the result of the other parameter if it is a Failure.
     * 
     * @param other
     */
    public abstract orElse(other: Func<T>): Try<T|any>;

    /**
     * Maps an instance of type T to an instance of type U by invoking function mapper.
     * 
     * @param mapper 
     */
    public abstract map<U>(mapper: Func<T, U>): Try<U>;

    /**
     * Maps an instance of type T to an instance of type Try by invoking the function mapper.
     * 
     * @param mapper 
     */
    public abstract flatMap<U>(mapper: Func<T, Try<U>>): Try<U>;

    /**
     * Recover from a Failure holding any exception to a Try.
     * 
     * @param recover 
     */
    public abstract recover(recover: Func<Error, T>): Try<T|any>;

    /**
     * Filter.
     * 
     * @param filter 
     */
    public abstract filter(filter: Func<T>): Try<T>;

    /**
     * Can set value on an object and return it.
     */
    public abstract set(setter: Func<T>): Try<T>;
}