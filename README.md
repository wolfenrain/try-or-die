[![pipeline status](https://gitlab.com/wolfenrain/try-or-die/badges/development/pipeline.svg)](https://gitlab.com/wolfenrain/try-or-die/commits/development) [![coverage report](https://gitlab.com/wolfenrain/try-or-die/badges/development/coverage.svg)](https://gitlab.com/wolfenrain/try-or-die/commits/development)
# try-or-die
Inspired by Scala's `Try` classes (including `Success` and `Failure`).

# Try and succeed
By using `try-or-die` you can avoid most try-catch blocks in your code. That is because `try-or-die` wraps around possible exceptions and simply returns the thing you want, or whatever else you want.

Don't get it? Here is an example that will always succeed:
```typescript
let result = Try<User>(() => userFactory.create('Jack'))
                .map(e => e.name)
                .map(e => e.substr(-1) === 'k')
                .value;

// Result will be true.
console.log(result);
```