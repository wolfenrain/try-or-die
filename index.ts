import { default as TryInvoke, Func, Options } from './src/Try';
import Success from './src/Success';
import Failure from './src/Failure';

const Try = <U>(mapper: Func<U>, opts?: Options): TryInvoke<U> => {
    opts = opts || {};
    try {
        let v = mapper();
        opts.class = v.constructor;

        return new Success<U>(v, opts);
    } catch(e) {
        return new Failure<U>(e, opts);
    }
}
export default Try;