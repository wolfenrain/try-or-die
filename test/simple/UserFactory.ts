import User from "./User";

export default class UserFactory {
    public create(name: string): User {
        return new User(name, new Date());
    }

    public createWithoutDefault(name: string): User {
        return User.withoutDefault(name, new Date());
    }
}