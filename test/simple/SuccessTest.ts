import 'mocha';
import { expect } from 'chai';
import Try from '../../index';
import User from './User';
import UserFactory from './UserFactory';

const userFactory = new UserFactory();

describe('Succes', () => {
    describe('#isSuccess', () => {
        it('should return true when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .isSuccess;
            
            expect(result).to.equal(true);
        });
    });
    describe('#isFailure', () => {
        it('should return false when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .isFailure;

            expect(result).to.equal(false);
        });
    });
    describe('#value', () => {
        it('should return User when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .value;
            
            expect(result).instanceof(User);
        });
    });
    describe('#get()', () => {
        it('should throw User when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .get();
            
            expect(result).instanceof(User);
        });
    });
    describe('#map()', () => {
        it('should return "Jack" when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map((e) => e.name)
                            .value

            expect(result).to.equal('Jack');
        })
    });
    describe('#flatMap()', () => {
        it('should return true when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map((e) => e.name)
                            .flatMap((e) => e.substr(-1) === 'k')
                            .value

            expect(result).to.equal(true);
        })
    });
    describe('#getOrDefault()', () => {
        it('should return "Jack" when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e)
                            .getOrDefault();
            
            expect(result.name).to.equal('Jack');
        });
    });
    describe('#getOrElse()', () => {
        it('should return "Jack" when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e)
                            .getOrElse(userFactory.create('John'))
            
            expect(result.name).to.equal('Jack');
        });
    });
    describe('#orElse()', () => {
        it('should return "Jack" when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e)
                            .orElse(() => userFactory.create('John'))
                            .value;
            
            expect(result.name).to.equal('Jack');
        });
    });
    describe('#recover()', () => {
        it('should return "Jack" when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e)
                            .recover(() => userFactory.create('John'));
            
            expect(result.isSuccess).to.equal(true);
            expect(result.get().name).to.equal('Jack');
        });
    });
    describe('#filter()', () => {
        it('should return true when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .filter(e => e.name === 'Jack');
            
            expect(result.isSuccess).to.equal(true);
        });
        it('should return false when fails to filter', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .filter(e => {});
            
            expect(result.isSuccess).to.equal(false);
        });
        it('should return false when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .filter(e => e.throwError());
            
            expect(result.isSuccess).to.equal(false);
        });
    });
    describe('#set()', () => {
        it('should return User when succeeds', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .set(e => e.name = 'John')
                            .value;
            
            expect(result).instanceof(User);
        });
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .set(e => e.throwError())
                            .value;
            
            expect(result).instanceof(Error);
        });
    });
});