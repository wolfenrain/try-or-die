export default class User {
    public name: string;

    public age: Date;

    public constructor(name: string, age: Date) {
        this.name = name;
        this.age = age;
    }

    public throwError() {
        throw new Error('Explicit error');
    }

    public static default() {
        return new User('John', new Date());
    }
    
    public static withoutDefault(name: string, age: Date): User {
        // Bit hacky but it works.
        let user: any = new User(name, age);
        delete user.constructor.default;
        
        return user;
    }
}