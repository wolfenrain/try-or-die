import 'mocha';
import { expect } from 'chai';
import Try from '../../index';
import User from './User';
import UserFactory from './UserFactory';

const userFactory = new UserFactory();

describe('Failure', () => {
    describe('#isSuccess', () => {
        it('should return false when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .isSuccess;
            
            expect(result).to.equal(false);
        });
    });
    describe('#isFailure', () => {
        it('should return true when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .isFailure;

            expect(result).to.equal(true);
        });
    });
    describe('#value', () => {
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .value;
            
            expect(result).instanceof(Error);
        });
    });
    describe('#get()', () => {
        it('should throw Error when fails', () => {
            try {
                // Ironic really
                let result = Try<User>(() => userFactory.create('Jack').throwError())
                                .get();
            } catch(e) {
                expect(e).instanceof(Error);
            }
        });
    });
    describe('#map()', () => {
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .map((e) => e.name)
                            .value

            expect(result).instanceof(Error);
        })
    });
    describe('#flatMap()', () => {
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .map((e) => e.name)
                            .flatMap((e) => e.substr(-1) === 'k')
                            .value

            expect(result).instanceof(Error);
        })
    });
    describe('#getOrDefault()', () => {
        it('should return true when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e.throwError())
                            .getOrDefault();
            
            expect(result.name).to.equal('John');
        });
        it('should return Error when no default', () => {
            let result = Try<User>(() => userFactory.createWithoutDefault('Jack'))
                            .map(e => e.throwError())
                            .getOrDefault();
            
            expect(result).instanceof(Error);
        });
    });
    describe('#getOrElse()', () => {
        it('should return true when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e.throwError())
                            .getOrElse(userFactory.create('John'))
            
            expect(result.name).to.equal('John');
        });
    });
    describe('#orElse()', () => {
        it('should return true when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e.throwError())
                            .orElse(() => userFactory.create('John'))
                            .value;
            
            expect(result.name).to.equal('John');
        });
    });
    describe('#recover()', () => {
        it('should return false when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e.throwError())
                            .recover(() => userFactory.create('John'));
            
            expect(result.isSuccess).to.equal(true);
            expect(result.get().name).to.equal('John');
        });
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack'))
                            .map(e => e.throwError())
                            .recover(() => userFactory.create('John').throwError());
            
            expect(result.isFailure).to.equal(true);
            expect(result.value).instanceof(Error);
        });
    });
    describe('#filter()', () => {
        it('should return false when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .filter(e => e.name === 'Jack');
            
            expect(result.isSuccess).to.equal(false);
        });
    });
    describe('#set()', () => {
        it('should return Error when fails', () => {
            let result = Try<User>(() => userFactory.create('Jack').throwError())
                            .set(e => e.name = 'John')
                            .value;
            
            expect(result).instanceof(Error);
        });
    });
});